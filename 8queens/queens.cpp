#include <iostream>
#include <fluents/Vector.hpp>
#include <fluents/PreservedIndexedArray.hpp>
#include <list>

namespace 
{
    bool isDiagonal(int x1, int y1, int x2, int y2)
    {
        const auto diff_x = x1 - x2;
        const auto diff_y = y1 - y2;
        return (diff_y % diff_x == 0 && std::abs(diff_y / diff_x) == 1);
    }

    bool isEmpty(int x)
    {
        return x == 0;
    }
}

namespace dojo
{
    class Queens
    {
    private:
        typedef std::list<std::vector<int>> ResultType;
        ResultType m_solutions;
        int m_size;

    public:
        Queens(int n) : m_size(n) { }

        bool isQueenAllowed(const fluents::PreservedIndexedArray<int>& board, int newRow, int newCol) const
        {
            return board.all_satisfies([=](int rowInBoard, int colInBoard) {
                return isEmpty(colInBoard) or ((newCol != colInBoard) and
                            not (isDiagonal(newRow, newCol, rowInBoard, colInBoard)));
            });
        }

        void obtainSolutions()
        {
            std::vector<int> base(m_size);
            m_solutions = tryPlacingQueen(fluents::PreservedIndexedArray<int>(base), 0);
        }

        std::list<std::vector<int>> tryPlacingQueen(const fluents::PreservedIndexedArray<int>& currentState, int rowToPlace) const
        {
            if (rowToPlace >= m_size) return {currentState.values()};

            return fluents::Vector<int>(m_size)
                .iota(1)
                .filter([&](int colToPlace) {
                    return isQueenAllowed(currentState, rowToPlace, colToPlace);
                })
                .map([&](int colToPlace) {
                    return tryPlacingQueen(currentState.set_value(rowToPlace, colToPlace), rowToPlace + 1);
                })
                .reduce([](ResultType result, const ResultType& curr) {
                    result.insert(result.end(), curr.begin(), curr.end());
                    return result;
                });
        }

        void printSolutions() const {
            for (const auto& line : m_solutions) {
                for (const auto& col : line) {
                    for (int i =1; i <= line.size(); i++) {
                        std::cout << ((i == col) ? "Q " : ". ");
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
        }

        std::size_t countSolutions() const {
            return m_solutions.size();
        }
    };
}

int main()
{
    dojo::Queens queens(8);
    queens.obtainSolutions();
    std::cout << "solution count: " << queens.countSolutions() << std::endl;
}
