#include <iostream>
#include <cmath>

int main()
{
    auto sum = 0;
    for (int i = 2; i <= 2000000; i++) {
        auto isPrime = true;
        for (auto mult = 2; mult <= 2000 && mult < i; mult++) {
            if (i % mult == 0) {
                isPrime = false;
                break;
            }
        }

        if (isPrime) {
            std::cout << i << std::endl;
            sum += i;
        }
    }

    std::cout << sum << std::endl;
}
