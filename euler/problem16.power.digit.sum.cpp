#include <iostream>
#include "../fluents/BigInteger.hpp"
#include <algorithm>
#include <numeric>

int main()
{
    using fluents::math::BigInteger;

    fluents::math::BigInteger a(9999);
    fluents::math::BigInteger b(1);
    fluents::math::BigInteger c(24);

    std::cout << a + b << std::endl;
    std::cout << a * 3 << std::endl;
    std::cout << BigInteger(2048) * 2048 << std::endl;
    std::cout << c * 3 << std::endl;


    BigInteger x(1);
    for(int i = 0; i < 1000; i++) 
    {
        x = x * 2;
    }
    std::cout << "2 to the 1000: " << x << std::endl;

    auto text = x.str();
    auto sum = std::accumulate(text.begin(), text.end(), 0,
                          [](const int sum, const int curr) { return sum + (curr - '0'); });
    std::cout << "sum of digits: " << sum << std::endl;

    BigInteger factorial(1);
    for (int i = 100 ; i >= 1; i--) {
        factorial = factorial * i;
    }

    /// of problem 20.  factorial digit sums
    auto text2 = factorial.str();
    auto sum2 = std::accumulate(text2.begin(), text2.end(), 0,
                          [](const int sum, const int curr) { return sum + (curr - '0'); });
    std::cout << "sum of digits in 100!: " << sum2 << std::endl;
}
