#include <iostream>
#include <iterator>
#include <fstream>
#include <string>
#include <vector>
#include "../fluents/Vector.hpp"
#include "../fluents/PreservedIndexedArray.hpp"
#include <numeric>


int main()
{
    std::fstream stream("files/p022_names.txt");
    std::vector<std::string> entries;

    std::string entry;
    while (std::getline(stream, entry, ',')) {
        entries.push_back(entry);
    }

    const auto crap = fluents::Vector<std::string>(entries)
        .map([](const std::string& name) {
            return name.substr(1, name.length() - 2); 
        })
        .sort().get();

    const auto indexed =
        fluents::PreservedIndexedArray<std::string>(crap)
        .map([](int index, const std::string& name) {
            auto score = std::accumulate(name.begin(), name.end(), 0, [](int sum ,char x) { return sum + (x - 'A' + 1); });
            return (index + 1) * score;
        }).values();

    const auto sum = fluents::Vector<int>(indexed)
        .reduce([](int sum, int curr) { return sum + curr;  });

    std::cout << "solution is: " << sum << std::endl;
//    std::cout << crap << std::endl;
//    std::cout << names.size() << std::endl;
}


