#define BOOST_TEST_MODULE MyTest
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <string>
#include <algorithm>
#include <numeric>

namespace
{
    std::string fizzBuzz(int max)
    {
        std::string result;
        for (int i = 1; i <= max; ++i)
        {
            if (i % 3 == 0 && i % 5 == 0) 
                result += "FizzBuzz\n";
            else if (i % 3 == 0) 
                result += "Fizz\n";
            else if (i % 5 == 0) 
                result += "Buzz\n";
            else
                result += std::to_string(i) + "\n";
        }

        return result;
    }
}

BOOST_AUTO_TEST_SUITE(Tests)
BOOST_AUTO_TEST_CASE(InvalidInputShallReturnEmptyString)
{
    BOOST_REQUIRE_EQUAL(fizzBuzz(0), "");
    BOOST_REQUIRE_EQUAL(fizzBuzz(-1), "");
};

BOOST_AUTO_TEST_CASE(InputOfOneShallReturnOneWithNewLine)
{
    BOOST_REQUIRE_EQUAL(fizzBuzz(1), "1\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(2), "1\n2\n");
}

BOOST_AUTO_TEST_CASE(FirstFizzAndBuzzOnMaxIs5)
{
    BOOST_REQUIRE_EQUAL(fizzBuzz(3), "1\n2\nFizz\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(4), "1\n2\nFizz\n4\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(5), "1\n2\nFizz\n4\nBuzz\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(6), "1\n2\nFizz\n4\nBuzz\nFizz\n");
}
BOOST_AUTO_TEST_CASE(FirstFizzAndBuzzOnMax9And10)
{
    BOOST_REQUIRE_EQUAL(fizzBuzz(9), "1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(10), "1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n");
    BOOST_REQUIRE_EQUAL(fizzBuzz(15), "1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz\n");
}

BOOST_AUTO_TEST_SUITE_END();
