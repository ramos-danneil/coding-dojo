#ifndef FLUENTS_MATH_BIGINTEGER_HPP
#define FLUENTS_MATH_BIGINTEGER_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <cassert>

namespace fluents { namespace math {

class BigInteger
{
public:
    BigInteger(int x) : m_text(std::to_string(x))
    {
    }

    BigInteger(const std::string& x) : m_text(x.empty() ? "0" : x )
    {
        auto pos = m_text.find_first_not_of("0");
        if (pos != std::string::npos)
        {
            m_text = m_text.substr(pos);
        }
    }

    BigInteger() : m_text("0")
    {
    }

    std::string str() const
    {
        return m_text;
    }

    BigInteger multiplyBy10()
    {
        return BigInteger(m_text + "0");
    }

private:
    std::string m_text;
};
namespace {
    std::string reverse(const std::string& str)
    {
        return std::string(str.rbegin(), str.rend());
    }

    int to_int(char x) { return x - '0'; };
    int to_char(int x) { return x + '0'; };
}

std::ostream& operator<< (std::ostream& os, const BigInteger& a)
{
    return os << a.str();
}

BigInteger operator+ (const BigInteger& a, const BigInteger& b)
{
    auto x = reverse(a.str());
    auto y = reverse(b.str());

    if (x.size() < y.size())
        x.swap(y);

    x += std::string("0"); // for padding;
    y += std::string(x.size() - y.size(), '0'); // for padding; 
    auto carry = 0;
    auto i = 0;
    for (;i < y.size(); i++)
    {
        auto sum = to_int(x[i]) + to_int(y[i]) + carry;
        x[i] = to_char(sum % 10);
        carry = sum / 10;
    }
    if (carry != 0) assert("SHIT");

    return BigInteger(reverse(x));
}

BigInteger operator* (const BigInteger& a, const BigInteger& b)
{
    auto x = reverse(a.str());
    auto y = reverse(b.str());

    if (x.size() > y.size()) x.swap(y);

    BigInteger finalProduct(0);

    for (int i = x.size() - 1; i >= 0; i--)
    {
        int carry = 0;
        std::stringstream partialStream;
        for (auto j = 0 ; j < y.size(); j++)
        {
            auto partial_product = (to_int(x[i]) * to_int(y[j])) + carry;
            auto num = partial_product % 10;
            carry = partial_product / 10;
            partialStream <<  num;
        }
        partialStream << carry;
        BigInteger partialProduct(reverse(partialStream.str()));
        finalProduct = finalProduct.multiplyBy10() + partialProduct;
    }
    
    return finalProduct;
}

}}

#endif //FLUENTS_MATH_BIGINTEGER_HPP
