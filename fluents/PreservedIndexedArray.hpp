#pragma once
#include <vector>
#include <algorithm>
#include <numeric>

namespace fluents {

template <typename T>
class PreservedIndexedArray
{
public:
    typedef T value_type;
    typedef typename std::vector<std::pair<int, T>> ContainerType;

public:
    explicit PreservedIndexedArray(const std::vector<T>& x) : m_contents(makeIndexedArray(x)) { };

    bool all_satisfies(const std::function<bool(int index, T value)>& func) const {
        for (const auto& entry : m_contents)
            if (not func(entry.first, entry.second))
                return false;
        return true;
    }

    template <typename U>
    ContainerType  makeIndexedArray(const std::vector<U>& x)
    {
        ContainerType temp;
        for (int i = 0; i < x.size(); ++i) {
            temp.emplace_back(i, x[i]);
        }
        return temp;
    }

    const PreservedIndexedArray filter(const std::function<bool(int index, T value)>& func) const {
        ContainerType new_contents;
        std::copy_if(m_contents.begin(), m_contents.end(), std::back_inserter(new_contents), 
                    [&func] (const typename ContainerType::value_type& entry) { return func(entry.first, entry.second); });

        return PreservedIndexedArray(std::move(new_contents));
    }
/*
   {

        std::vector<ReturnType> result;
        std::transform(std::begin(m_contents), std::end(m_contents), std::back_inserter(result), func);
        return Vector<ReturnType>(std::move(result));
    }
*/

    template <typename Callable>
    auto map(Callable&& func) const -> PreservedIndexedArray<decltype(func(0, std::declval<T>()))>
    {
        typedef decltype(func(0, std::declval<T>())) ReturnType;
        typedef std::vector<ReturnType> ReturnContainerType;

        ReturnContainerType new_contents;

        std::transform(std::begin(m_contents), std::end(m_contents),
            std::back_inserter(new_contents), [&func](const typename ContainerType::value_type& entry) {
               return func(entry.first, entry.second);
            });

        return PreservedIndexedArray<ReturnType>(new_contents);
    }

    const PreservedIndexedArray reflow() const {
        ContainerType new_contents(m_contents);
        for (auto index = 0; index < m_contents.size(); ++index) {
            new_contents[index].first = index;
        }
        return PreservedIndexedArray(std::move(new_contents));
    }

    const PreservedIndexedArray sort(std::function<bool(const T& a, const T& b)> comparator= std::less<T>()) const {
        ContainerType new_contents(m_contents);
        std::sort(std::begin(m_contents), std::end(m_contents),
                  [&comparator](const typename ContainerType::value_type& a, const typename ContainerType::value_type& b) {
                       return comparator(a.second, b.first); 
                  });
        return PreservedIndexedArray(std::move(new_contents));
    }

    const std::size_t get_index_of_first(const T& value) const {
        auto pos = std::find_if(std::begin(m_contents), std::end(m_contents),
                        [&value](const typename ContainerType::value_type& entry) {
                            return entry.second == value;
                        });
        return (pos == std::end(m_contents)) ? -1 : pos->first;
    }

    const std::size_t size() const {
        return m_contents.size();
    }

    void for_each(std::function<void(int, const T& value)> func) const {
        for (const auto& entry : m_contents) {
            func(entry.first, entry.second);
        }
    }

    const PreservedIndexedArray set_value(int index, const T& value) const {
        ContainerType new_contents(m_contents);
        new_contents[index] = std::make_pair(index, value);
        return PreservedIndexedArray(std::move(new_contents));
    }

    std::vector<T> values() const {
        std::vector<T> values;
        std::transform(std::begin(m_contents), std::end(m_contents), std::back_inserter(values),
                        [](const typename ContainerType::value_type& entry) { return entry.second; });
        return values;
    }

private:
    PreservedIndexedArray(const ContainerType& that) : m_contents{that} { };
    PreservedIndexedArray(ContainerType&& that) : m_contents{that} { };
    const ContainerType m_contents;
};

}
