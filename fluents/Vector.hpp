#pragma once
#include <vector>
#include <algorithm>
#include <numeric>
#include <functional>
#include <ostream>

namespace fluents
{
    template <typename T>
    class Vector;

    template <typename U>
    std::ostream& operator<<(std::ostream& ss, const Vector<U>& vector)
    {
        ss << "[";
        for (const auto& element : vector.m_contents) {
            ss << element << ", ";
        }
        ss << "]";
        return ss;
    }

    template <typename T>
    class Vector
    {
    public:
        typedef std::vector<T> ContainerType;
        Vector(int size) : m_contents(size)
        {
        }

        Vector(const ContainerType& that) : m_contents{that} { }

        Vector(ContainerType&& that) : m_contents{that} { }

    public:

        Vector iota(int start = 0) const {
            ContainerType new_contents {m_contents};
            std::iota(new_contents.begin(), new_contents.end(), start);
            return Vector(std::move(new_contents));
        }

        void for_each(const std::function<void(T)>& func) const {
            for (const auto& i : m_contents) {
                func(i);
            }
        }

        template <typename Callable>
        auto map(Callable&& func) const -> Vector<decltype(func(std::declval<T>()))>
        {
            typedef decltype(func(std::declval<T>())) ReturnType;

            std::vector<ReturnType> result;
            std::transform(std::begin(m_contents), std::end(m_contents), std::back_inserter(result), func);
            return Vector<ReturnType>(std::move(result));
        }

        const Vector<T> filter(std::function<bool(T)> func) const {
            ContainerType new_contents;
            std::copy_if(m_contents.begin(), m_contents.end(), std::back_inserter(new_contents), func);
            return Vector(std::move(new_contents));
        }

        const Vector<T> sort() const {
            ContainerType new_contents = m_contents;
            std::sort(new_contents.begin(), new_contents.end());
            return Vector(std::move(new_contents));
        }

        T reduce(std::function<T(T, const T&)> func) const
        {
            return std::accumulate(m_contents.begin(), m_contents.end(), T{}, func);
        }

        std::size_t size() const 
        {
            return m_contents.size();
        }

        const ContainerType& get() const 
        {
            return m_contents;
        }
        
        friend std::ostream& operator<< <> (std::ostream& ss, const Vector<T>& vector);

    private:
        std::vector<T> m_contents;
    };




}
