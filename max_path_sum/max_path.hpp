#pragma once
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <fstream>
#include <iterator>
#include <sstream>
#include <iostream>
#include <valarray>

namespace dojo
{
    class MaxPathSum
    {
    public:
        std::vector<int> getNextRow(const std::vector<int>& current,
                                    const std::vector<int>& next) const {
            if (next.size() != current.size() + 1)
                throw std::logic_error("next row should be 1 greater");

            std::vector<int> result(next.begin(), next.end());
            const auto max = next.size();
            for (auto i = 0; i < next.size(); ++i) {
                result[i] = next[i] + std::max((i > 0) ? current[i - 1] : 0,
                                               (i < current.size()) ? current[i] : 0);
            }
            return result;
        }

        int readFromFile(const std::string& filePath)
        {
            std::ifstream stream(filePath);
            std::string line;
            std::vector<int> states;

            while(std::getline(stream, line)) {
                std::vector<int> nums;
                std::istringstream iss(line);
                std::copy(std::istream_iterator<int>(iss),
                          std::istream_iterator<int>(),
                          std::back_inserter(nums));
                states = getNextRow(states, nums);
            }
            return *std::max_element(states.begin(), states.end());
        }
    };
}
