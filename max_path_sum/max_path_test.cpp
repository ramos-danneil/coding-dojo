//#define BOOST_TEST_MODULE max_path_sum
#include <iostream>
#include <cassert>
#include "max_path.hpp"
//#include <boost/test/test_tools.hpp>

//##include <boost/test/included/unit_test.hpp>
#include <type_traits>


namespace
{
    /*
    void check_vectors(const std::vector<int>& a, const std::vector<int>& b)
    {
        BOOST_CHECK_EQUAL_COLLECTIONS(a.begin(), a.end(), b.begin(), b.end());
    }
    */
}

//BOOST_AUTO_TEST_CASE(Sample)
//{
int main()
{
    dojo::MaxPathSum maxPathSum;
/*
    check_vectors(maxPathSum.getNextRow({0}, {1,2}), {1,2});
    check_vectors(maxPathSum.getNextRow({1}, {3,2}), {4,3});
    check_vectors(maxPathSum.getNextRow({4,3}, {1,5,3}), {5,9,6});
*/

    auto x = maxPathSum.readFromFile("input.txt");
    std::cout << "max path sum: " << x << std::endl;
}
